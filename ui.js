// ===== Données ======
const width_case = 60;
const width_trou = 15;
const height_trou = 30;
const x_origin = -((width_case + 10) * 19);
const table = document.querySelector("#transitions");
const dropArea = document.querySelector('#drop-area');
const fileElem = document.querySelector("#file");
const popup = document.querySelector("#popup");

let machine = null;
let mot = null;
let etat = null;
let iCase = null;
let cases = null;
// ==============================
//       CANVAS 1
// ==============================

const canvas1 = document.getElementById("canvas1");
const stage1 = new createjs.Stage(canvas1);
stage1.enableMouseOver();
createjs.Touch.enable(stage1);
stage1.mouseMoveOutside = true;

createjs.Ticker.on("tick", stage1);

let container = null;
let text = null;

function prepare(){
    stage1.removeAllChildren();
    stage1.update();
    // ===== ETAT =====
    const eContainer= new createjs.Shape();
    stage1.addChild(eContainer);
    eContainer.x = (width_case + 10) * 2.2;
    eContainer.y = 0;
    eContainer.graphics.s('#000').ss(1).f('#ECECED').r(0, 0, width_case, width_case).ef();
    eContainer.shadow = new createjs.Shadow('#333', 1, 1, 2);

    text = new createjs.Text(etat, "30px Verdana", "#000");
    stage1.addChild(text);
    var bounds = text.getMetrics();
    text.x = eContainer.x + bounds.width / 4 + 4;
    text.y = bounds.height / 2;

    const labelEtat = new createjs.Text("Registre d'état", "20px Verdana", "#000");
    stage1.addChild(labelEtat);
    bounds = labelEtat.getMetrics();
    labelEtat.x = 0;
    labelEtat.y = width_case / 2 - bounds.height / 2;

    // ===== VARIABLE A =====
    table.children[1].innerHTML = "";
    for (const [key, value] of Object.entries(machine.actions)) {
        for (const [lu, action] of Object.entries(value)){
            let tr = document.createElement('tr');
            tr.id = key + "|" + lu;
            let inner = `<td>${lu}</td><td>${action.ecrit}</td><td>${action.deplacement}</td><td>${action.etat}</td>`;
            tr.innerHTML = `<td>${key}</td>` + inner;
            table.children[1].appendChild(tr);
        }
    }
    document.getElementById(etat + "|" + mot[25]).classList.toggle("selected");
    const textRuban = new createjs.Text("Ruban", "20px Verdana", "#000");
    stage1.addChild(textRuban);
    bounds = textRuban.getMetrics();
    textRuban.x = 0;
    textRuban.y = canvas1.height - 2 - (3 * height_trou) - 30 - width_case - 20;

    const ligne_haute =  new createjs.Shape();
    stage1.addChild(ligne_haute);
    ligne_haute.graphics.s('#000').ss(2).f('#FFF').mt(0, canvas1.height - 2 - (3 * height_trou) - 30 - width_case).lt(900, canvas1.height - 2 - (3 * height_trou) - 30 - width_case);
    const ligne_basse =  new createjs.Shape();
    stage1.addChild(ligne_basse);
    ligne_basse.graphics.s('#000').ss(2).f('#FFF').mt(0, canvas1.height - 2).lt(900, canvas1.height - 2);

    container = new createjs.Container();
    stage1.addChild(container);
    for (let i = 0; i <= ((width_case + 10) * 51); i ++){
        let trou = new createjs.Shape();
        container.addChild(trou);
        trou.x = x_origin + (width_trou + 10) * i;
        trou.y = canvas1.height - 2 - (3 * height_trou) - 20 - width_case;
        trou.graphics.s('#000').ss(1).f('#FFF').r(0, 0, width_trou, height_trou).ef();
        trou = new createjs.Shape();
        container.addChild(trou);
        trou.x = x_origin + (width_trou + 10) * i;
        trou.y = canvas1.height - 2 - height_trou - 10;
        trou.graphics.s('#000').ss(1).f('#FFF').r(0, 0, width_trou, height_trou).ef();
    }

    for (let i = 0; i < 51; i++){
        const bacA = new createjs.Shape();
        container.addChild(bacA);
        bacA.x = x_origin + (width_case + 10) * i;
        bacA.y = canvas1.height - 2 - (2 * height_trou) - width_case;
        bacA.graphics.s('#000').ss(1).f('#ECECED').r(0, 0, width_case, width_case).ef();
        bacA.shadow = new createjs.Shadow('#333', 1, 1, 2);
        const text = new createjs.Text(mot[i], "40px Verdana", "#000");
        cases.push(text);
        container.addChild(text);
        var bounds = text.getMetrics();
        text.x = x_origin + (width_case + 10) * i + (bounds.width / 2) + 4;
        text.y = canvas1.height - 2 - (2 * height_trou) - width_case + bounds.height / 2 - 6;
    }

    const tete = new createjs.Shape();
    stage1.addChild(tete);
    tete.graphics.ss(2).s('#000').f("#C00").mt(canvas1.width / 2 - 5, 50).lt(canvas1.width / 2 - 5, 160).lt(canvas1.width / 2, 185).lt(canvas1.width / 2 + 5, 160).lt(canvas1.width / 2 + 5, 50).cp();
    tete.graphics.ss(2).s('#000').f("#C00").mt(canvas1.width / 2 - 100, 0).lt(canvas1.width / 2 - 80, 50).lt(canvas1.width / 2 + 80, 50).lt(canvas1.width / 2 + 100, 0).cp();
    stage1.update();
    let textTete = new createjs.Text("Tête de", "18px Verdana", "#ECECED");
    stage1.addChild(textTete);
    bounds = textTete.getMetrics();
    textTete.x = canvas1.width / 2 - bounds.width / 2;
    textTete.y = 6;
    textTete = new createjs.Text("Lecture / Écriture", "18px Verdana", "#ECECED");
    stage1.addChild(textTete);
    let bounds2 = textTete.getMetrics();
    textTete.x = canvas1.width / 2 - bounds2.width / 2;
    textTete.y = bounds.height + 8;

}

document.getElementById("executer").addEventListener('click', function(){
    if (!machine.finaux.includes(etat)){
        document.querySelector(".selected").classList.toggle("selected");
        
        let valeur = mot[iCase];
        let action = machine.actions[etat][valeur];
        mot[iCase] = action.ecrit;
        cases[iCase].text = action.ecrit;
        
        if (action.deplacement == "gauche"){
            createjs.Tween.get(container, {override:true}).to({x: container.x - width_case - 10}, 200);
            iCase++;
        } else {
            createjs.Tween.get(container, {override:true}).to({x: container.x + width_case + 10}, 200);
            iCase--;
        }
        etat = action.etat;
        text.text = etat;
        document.getElementById(etat + "|" + mot[iCase]).classList.add("selected");
    }
});

function load(machineTuring){
    machine = machineTuring;
    mot = " ".repeat(25) + machine.mot + " ".repeat(25 - machine.mot.length + 1)
    etat = machine.initial;
    iCase = 25;
    cases = [];
    prepare();
}

let exemple = {
  "initial" : "e1",
  "mot" : "1011",
  "finaux" : ["fin"],
  "actions" : {
    "e1" : { 
      "0" : { "ecrit" : "0", "deplacement" : "gauche", "etat" : "e1"},
      "1" : { "ecrit" : "1", "deplacement" : "gauche", "etat" : "e1"},
      " " : { "ecrit" : "0", "deplacement" : "gauche", "etat" : "e2"}
    },
    "e2" : {
      " " : { "ecrit" : " ", "deplacement" : "droite", "etat" : "fin"}
    }
  }
};

load(exemple);

document.querySelector("#reinitialiser").addEventListener('click', (e) => {
    load(machine);
});


fileElem.addEventListener('change',function(){
	handleFile(this.files[0]);
 });

['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, preventDefaults, false);
  });
  
  function preventDefaults (e) {
	e.preventDefault();
	e.stopPropagation();
  }
  
  ['dragenter', 'dragover'].forEach(eventName => {
	dropArea.addEventListener(eventName, highlight, false);
  });
  
  ['dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, unhighlight, false);
  });
  
  function highlight(e) {
	dropArea.classList.add('highlight');
  }
  
  function unhighlight(e) {
	dropArea.classList.remove('highlight');
  }
  
dropArea.addEventListener('drop', handleDrop, false)

function handleFile(file){
	let fileReader = new FileReader();

	fileReader.onload = function(){
		const extension = file.name.split('.').pop().toLowerCase();
		if (extension !== "json"){
			alert("Format de fichier non autorisé. Seul le format JSON est autorisé.");
		} else {
			load(JSON.parse(fileReader.result));
      alert("Machine de turing chargée.");
		}
	}

  	fileReader.readAsText(file);
}

function handleDrop(e) {
	let dt = e.dataTransfer;
	let file = dt.files[0];
	handleFile(file);
}

popup.addEventListener('click', function(){
  this.style.display = "none";
});


document.querySelector("#aide").addEventListener('click', function(){
    document.querySelector("#popup-message").innerHTML = "<div class='block'> <h1>Machine de Turing</h1> <p>Une machine de turing est un modèle théorique défini par Alan Turing en 1936.</p> <p>Elle permet de modéliser le concept d’algorithme, comme une procédure mécanique permettant de résoudre un problème.</p> <p>Une machine de Turing comporte les éléments suivants :</p> <ul> <li>Un <b>ruban infini</b> divisé en cases consécutives.<ul><li>Chaque case contient un symbole d'un alphabet fini donné, en l'occurrence {0, 1, ' '} où ' ' est le symbole vide,</li><li>Le ruban est initialisé avec un <b>mot</b>, i.e une suite de symboles de l'alphabet.</li></ul></li> <li>Une <b>tête de lecture/écriture</b> qui peut lire et écrire les symboles sur le ruban, </li> <li>Un <b>registre d'état</b> qui mémorise l'état courant de la machine de Turing. <ul> <li>Le nombre d'états possibles est toujours fini, </li> <li>Il existe un « état de départ » qui est l'état initial de la machine avant son exécution,</li> <li>Il existe au moins un « état final » qui arrête la machine lorsque celle-ci atteint cet état.</li> </ul> </li> <li>Une <b>table d'actions</b> qui indique à la machine quel symbole écrire sur le ruban, comment déplacer le ruban et quel est le nouvel état, en fonction du symbole lu sur le ruban et de l'état courant de la machine.</li> </ul> </div>";
    popup.style.display = "flex";
});

document.querySelector("#actions").addEventListener('click', function(){
  document.querySelector("#popup-message").innerHTML = "<div class='block'><h1>Actions possibles de l'interface</h1><ul> <li><label class='button popup'>Importer</label> : Charge une machine de Turing à partir d'un fichier JSON.<ul style='list-style: none; padding: 0;'><li>Exemple d'une machine au format JSON : <pre><code>{\n  'initial' : 'e1',\n  'mot' : '1011',\n  'finaux' : ['fin'],\n  'actions' : {\n    'e1' : {\n      '0' : {'ecrit' : '0', 'deplacement' : 'gauche', 'etat' : 'e1'}, \n      '1' : {'ecrit' : '1', 'deplacement' : 'gauche', 'etat' : 'e1'},\n      ' ' : {'ecrit' : '0', 'deplacement' : 'gauche', 'etat' : 'e2'}\n    },'e2' : {\n      ' ' : {'ecrit' : ' ', 'deplacement' : 'droite', 'etat' : 'fin'}\n    }\n  }\n}</code></pre></li></ul></li><li><label class='button popup'>Exécuter</label> : Exécute un pas de la machine de Turing,</li> <li><label class='button popup'>Réinitialiser</label> : Place la machine de Turing dans son état initial (Registre d'état, mot initial sur le ruban, position de la tête de lecture).</li> </ul> </div>";
  popup.style.display = "flex";
});